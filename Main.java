package Controladores;

import java.util.ArrayList;
import java.util.List;


import Modelos.UsuarioModelo;
import XML.Xml;
import pruebaDos.Usuario;

public class Main {
	
	
	/**
	 * Esta clase es la principal
	 * @author:Irene Lorza
	 * @version: 2017-01-05/1
	 */
	public static void main (String[] args) {
		Xml xml = new Xml();
		UsuarioModelo usuarioModelo = new UsuarioModelo();
		int x = xml.crearXml();
		if (x ==1) {
			List<Usuario> listaUsuario = new ArrayList<Usuario>();
			listaUsuario = xml.leerXML();
			int y =usuarioModelo.insertarUsuario (listaUsuario);
			if (y==1) {
				System.out.println("Se a generado el xml y se a extraido informacion, insertando en BD " + x);
			}
		}
	}
	
	
}
