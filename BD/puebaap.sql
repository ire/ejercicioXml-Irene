-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.6.16 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para puebaap
CREATE DATABASE IF NOT EXISTS `puebaap` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `puebaap`;

-- Volcando estructura para tabla puebaap.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `PK_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) DEFAULT NULL,
  `tipo` varchar(150) DEFAULT NULL,
  `requerido` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`PK_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla puebaap.usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`PK_usuario`, `descripcion`, `tipo`, `requerido`) VALUES
	(12, 'schemaLocation', 'xs:string', NULL),
	(13, 'schemaLocation', 'xs:string', NULL),
	(14, 'ExchangeRate', 'xs:string', 'required'),
	(15, 'schemaLocation', 'xs:string', NULL),
	(16, 'InternalInvoiceNumber', 'xs:string', 'required'),
	(17, 'CustomerCode', 'xs:string', NULL),
	(18, 'LegalEntityName', 'xs:string', 'required'),
	(19, 'email', 'xs:string', 'required'),
	(20, 'Currency', 'xs:string', NULL),
	(21, 'PONumber', 'xs:string', NULL),
	(22, 'ExchangeRate', 'xs:string', 'required'),
	(23, 'schemaLocation', 'xs:string', NULL),
	(24, 'InternalInvoiceNumber', 'xs:string', 'required'),
	(25, 'CustomerCode', 'xs:string', NULL),
	(26, 'LegalEntityName', 'xs:string', 'required'),
	(27, 'email', 'xs:string', 'required'),
	(28, 'Currency', 'xs:string', NULL),
	(29, 'PONumber', 'xs:string', NULL),
	(30, 'ExchangeRate', 'xs:string', 'required'),
	(31, 'schemaLocation', 'xs:string', NULL),
	(32, 'InternalInvoiceNumber', 'xs:string', 'required'),
	(33, 'CustomerCode', 'xs:string', NULL),
	(34, 'LegalEntityName', 'xs:string', 'required'),
	(35, 'email', 'xs:string', 'required'),
	(36, 'Currency', 'xs:string', NULL),
	(37, 'PONumber', 'xs:string', NULL),
	(38, 'ExchangeRate', 'xs:string', 'required'),
	(39, 'schemaLocation', 'xs:string', NULL),
	(40, 'InternalInvoiceNumber', 'xs:string', 'required'),
	(41, 'CustomerCode', 'xs:string', NULL),
	(42, 'LegalEntityName', 'xs:string', 'required'),
	(43, 'email', 'xs:string', 'required'),
	(44, 'Currency', 'xs:string', NULL),
	(45, 'PONumber', 'xs:string', NULL),
	(46, 'ExchangeRate', 'xs:string', 'required'),
	(47, 'schemaLocation', 'xs:string', NULL),
	(48, 'InternalInvoiceNumber', 'xs:string', 'required'),
	(49, 'CustomerCode', 'xs:string', NULL),
	(50, 'LegalEntityName', 'xs:string', 'required'),
	(51, 'email', 'xs:string', 'required'),
	(52, 'Currency', 'xs:string', NULL),
	(53, 'PONumber', 'xs:string', NULL),
	(54, 'ExchangeRate', 'xs:string', 'required'),
	(55, 'schemaLocation', 'xs:string', NULL),
	(56, 'InternalInvoiceNumber', 'xs:string', 'required'),
	(57, 'CustomerCode', 'xs:string', NULL),
	(58, 'LegalEntityName', 'xs:string', 'required'),
	(59, 'email', 'xs:string', 'required'),
	(60, 'Currency', 'xs:string', NULL),
	(61, 'PONumber', 'xs:string', NULL),
	(62, 'ExchangeRate', 'xs:string', 'required'),
	(63, 'schemaLocation', 'xs:string', NULL),
	(64, 'InternalInvoiceNumber', 'xs:string', 'required'),
	(65, 'CustomerCode', 'xs:string', NULL),
	(66, 'LegalEntityName', 'xs:string', 'required'),
	(67, 'email', 'xs:string', 'required'),
	(68, 'Currency', 'xs:string', NULL),
	(69, 'PONumber', 'xs:string', NULL),
	(70, 'ExchangeRate', 'xs:string', 'required'),
	(71, 'schemaLocation', 'xs:string', NULL),
	(72, 'InternalInvoiceNumber', 'xs:string', 'required'),
	(73, 'CustomerCode', 'xs:string', NULL),
	(74, 'LegalEntityName', 'xs:string', 'required'),
	(75, 'email', 'xs:string', 'required'),
	(76, 'Currency', 'xs:string', NULL),
	(77, 'PONumber', 'xs:string', NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
