package Modelos;

import java.util.List;

import javax.persistence.EntityManager;

import libs.Conexion;
import pruebaDos.Usuario;
public class UsuarioModelo {
	
	/**
	 * Esta clase se encarga de recorrer lista e insertar los objetos a la bd.
	 * @author:Irene Lorza
	 * @version: 2017-01-05/1
	 * @throws: Para cachar algun error durante la transacción se ocupa try catch. 
	 * @param: List de tipo Usuario (listaUsuario)
	 * @return: retorna valor 1 si se realizo la transaccion con exito de lo contrario el valor sera 0.
	 */
	public int insertarUsuario (List<Usuario> listaUsuario) {
		int x=0;
		try {
			Conexion conexion = new Conexion();
			EntityManager entityManajer = conexion.conexion();		
			if (listaUsuario != null) {
				entityManajer.getTransaction().begin();
				for (int i=0; i<listaUsuario.size(); i++) {
					Usuario usuario =(Usuario) listaUsuario.get(i);
					entityManajer.persist(usuario);
				}
				entityManajer.getTransaction().commit(); 
			}
			x=1;
		} catch (Exception e) {
			System.out.println("insertarUsuario e: " +e);
			x=0;
		}
		return x;
	}
}
