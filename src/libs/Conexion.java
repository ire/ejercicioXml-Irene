package libs;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Conexion {
	EntityManagerFactory entityManagerFactory;
	EntityManager entityManajer;
	
	/**
	 * Metodo que hace la persistencia con EntityManagerFactory, y poder conectarse entre entidades (pojos),
	 * y con EntityManager se manipula y hace la conexion entre los dos
	 * @author:Irene Lorza
	 * @version: 2017-01-05/1
	 * @return: retorna EntityManager.
	 */
	public EntityManager conexion () {
		entityManagerFactory = Persistence.createEntityManagerFactory("pruebaDos");
		entityManajer = entityManagerFactory.createEntityManager();
		return entityManajer;
	}
	
}
