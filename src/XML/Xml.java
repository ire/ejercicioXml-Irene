package XML;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jdom2.input.SAXBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import pruebaDos.Usuario;

public class Xml {
	
	
	/**
	 * Esta clase se encarga de construir un xml, generar el archivo xml y lo guarda 
	 * dentro del proyecto 
	 * @author:Irene Lorza
	 * @version: 2017-01-05/1
	 * @throws: Para cachar algun error al construir el xml se ocupa try catch 
	 * @return: Retorna valor 1 si se genero el xml, de lo contrario retorna el valor de 0.
	 */
	public int crearXml () {
		int x=0;
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document XML = docBuilder.newDocument();
			Element ElmtSchema = XML.createElementNS("http://www.w3.org/2001/XMLSchema","xs:schema");
				ElmtSchema.setAttribute("xmlns:xs",  "http://www.w3.org/2001/XMLSchema");
				ElmtSchema.setAttribute("elementFormDefault", "qualified"); 
				Element ElmtElement = XML.createElementNS("http://www.w3.org/2001/XMLSchema","xs:element");
					ElmtElement.setAttribute("name", "AP");
				
					Element ElmtContexType = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:complexType");
					ElmtElement.appendChild(ElmtContexType);
					Element ElmtAttribute = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttribute.setAttribute("name", "schemaLocation");
						ElmtAttribute.setAttribute("type","xs:string");
					ElmtContexType.appendChild(ElmtAttribute);
					Element ElmtAttributeUno = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeUno.setAttribute("name", "PONumber");
						ElmtAttributeUno.setAttribute("type","xs:string");
					ElmtContexType.appendChild(ElmtAttributeUno);
					Element ElmtAttributeDos = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeDos.setAttribute("name", "email");
						ElmtAttributeDos.setAttribute("type","xs:string");
						ElmtAttributeDos.setAttribute("use","required");
					ElmtContexType.appendChild(ElmtAttributeDos);
					Element ElmtAttributeTres = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeTres.setAttribute("name", "LegalEntityName");
						ElmtAttributeTres.setAttribute("type","xs:string");
						ElmtAttributeTres.setAttribute("use","required");
					ElmtContexType.appendChild(ElmtAttributeTres);
					Element ElmtAttributeCuatro = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeCuatro.setAttribute("name", "CustomerCode");
						ElmtAttributeCuatro.setAttribute("type","xs:string");
					ElmtContexType.appendChild(ElmtAttributeCuatro);
					Element ElmtAttributeCinco = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeCinco.setAttribute("name", "Currency");
						ElmtAttributeCinco.setAttribute("type","xs:string");
					ElmtContexType.appendChild(ElmtAttributeCinco);
					Element ElmtAttributeSeis = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeSeis.setAttribute("name", "ExchangeRate");
						ElmtAttributeSeis.setAttribute("type","xs:string");
						ElmtAttributeSeis.setAttribute("use","required");
					ElmtContexType.appendChild(ElmtAttributeSeis);
					Element ElmtAttributeSiete = XML.createElementNS("http://www.w3.org/2001/XMLSchema", "xs:attribute");
						ElmtAttributeSiete.setAttribute("name", "InternalInvoiceNumber");
						ElmtAttributeSiete.setAttribute("type","xs:string");
						ElmtAttributeSiete.setAttribute("use","required");
					ElmtContexType.appendChild(ElmtAttributeSiete);
				ElmtSchema.appendChild(ElmtElement);
			XML.appendChild(ElmtSchema);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer;
			transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(XML);	
			StreamResult result = new StreamResult(new File("sanmina.xml"));
			transformer.transform(source, result); 
			x=1;
		} catch (Exception e) {
			x=0;
			System.out.println("crearXml e: " +e);
		}
		return x;
		
	}
	
	/**
	 *  Esta clase se encarga de obtener el archivo que se guardo en el proyecto con el metodo 
	 *  crearXml, y lo recorre para obtener los datos y generar una lista de tipo Usuario.
	 * @author:Irene Lorza
	 * @version: 2017-01-05/1
	 * @throws: Para cachar algun error al leer el xml se ocupa try catch. 
	 * @return: Retorna lista de tipo Usuario (listaUsuario).
	 */
	public List<Usuario> leerXML () {
		List<Usuario> listaUsuario = new ArrayList<Usuario>();
		try {
			SAXBuilder cargarXml = new SAXBuilder();
			File xmlFile = new File( "sanmina.xml" );
			org.jdom2.Document document = cargarXml.build(xmlFile);
			System.out.println("leer archivo --- " + document.getRootElement());
			org.jdom2.Element elemntXml = document.getRootElement();
			System.out.println("Children ---- " + elemntXml.getChildren());
			List<org.jdom2.Element> lista = elemntXml.getChildren();  
			System.out.println("Children 1---- " + lista);
			if (lista != null) {
				for(org.jdom2.Element hijo: lista){  
					System.out.println("\nEtiqueta: "+hijo.getName()+". Texto: "+hijo.getValue());  
					if (hijo.getName().equals("element")) {
						List<org.jdom2.Element> listaComplexType = hijo.getChildren(); 
						if (listaComplexType != null) {
							for (int i = 0; i < listaComplexType.size(); i++) {
								org.jdom2.Element elemtComplext = (org.jdom2.Element) listaComplexType.get(i);
								if (elemtComplext.getName().equals("complexType")) {
									List<org.jdom2.Element> listaAttribute = elemtComplext.getChildren();
									if (listaAttribute != null) {
										for (int x= 0; x<listaAttribute.size(); x++){
											org.jdom2.Element elemtAttribute = (org.jdom2.Element) listaAttribute.get(x);
											Usuario usuario = new Usuario();
											if (elemtAttribute.getAttribute("name") != null) {
												usuario.setDescripcion(elemtAttribute.getAttribute("name").getValue());
//												System.out.println("name " + x +": " + elemtAttribute.getAttribute("name").getValue());
											} 
											if (elemtAttribute.getAttribute("type") != null) {
												usuario.setTipo(elemtAttribute.getAttribute("type").getValue());
//												System.out.println("type " + x +": " + elemtAttribute.getAttribute("type").getValue());
											}
											if (elemtAttribute.getAttribute("use") != null) {
												usuario.setRequerido(elemtAttribute.getAttribute("use").getValue());
//												System.out.println("use " + x +": " + elemtAttribute.getAttribute("use").getValue());
											}
											listaUsuario.add(usuario);
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("leerXML e: "+e);
		}
		return listaUsuario;
	}
	
	
}
