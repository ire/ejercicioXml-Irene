package pruebaDos;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the usuarios database table.
 * 
 */
@Entity
@Table(name="usuarios")
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int PK_usuario;

	private String descripcion;

	private String requerido;

	private String tipo;

	public Usuario() {
	}

	public int getPK_usuario() {
		return this.PK_usuario;
	}

	public void setPK_usuario(int PK_usuario) {
		this.PK_usuario = PK_usuario;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getRequerido() {
		return this.requerido;
	}

	public void setRequerido(String requerido) {
		this.requerido = requerido;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}